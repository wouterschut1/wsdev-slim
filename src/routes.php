<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);

    
});

$app->get('/test',function ($request, $response) {
   


    return $this->view->render($response, 'test.phtml', $csrf_tokens);
    
})->add($container->get('csrf'))->setName('index');